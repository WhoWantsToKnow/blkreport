#!/usr/bin/env python3
"""
   blkreport.py

   Copyright 2016 Keith Watson <swillber@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.
"""
#
#===============================================================================
#
#  Coding Style:
#
#     Variables - capitalized with camel caps
#     Constants - all uppercase
#     In-line functions - as for variables but with 'fn' prefix
#     Lists - as for variables but with 'list' prefix
#     Dictionaries - as for variables but with 'dict' prefix
#===============================================================================
#
#  Primary Data Structures:
#
#===============================================================================
#
#  Report Format:
#
#===============================================================================
#
import os
import sys
import argparse
import traceback
import sh            # https://amoffat.github.io/sh/
import tempfile
#
# get list of current block devices known to system
#
def fnGetDevicesList():
    listDevices = []
    try:
        Devices = sh.lsblk("-ldno", "NAME", e="11")
        # get list of all block devices currently accessible (exclude CD/DVD drive)
        # e.g. returns list of blk devices
        # sda
        # sdb
        # sdc
    except ErrorReturnCode:
        ExitCode = str(Devices.exit_code)
        print("ERROR: lsblk in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
        sys.exit(1)
    for Device in Devices.splitlines():
        listDevices.append(Device)
    return listDevices
#
#
#
def fnGetDeviceData(Device):
    DevAddr = "/dev/"+Device
    dictDevInfo = {}
    #
    try:
        DevInfo = sh.lsblk("-aldbno", "SIZE,VENDOR,SERIAL,MODEL", DevAddr)
        # get minimal information for a device
        # e.g. 128035676160 ATA      S2HUNXBG807446 SAMSUNG MZNLF128
    except ErrorReturnCode:
        ExitCode = str(DevInfo.exit_code)
        print("ERROR: lsblk in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
        sys.exit(1)
    listDevInfo = DevInfo.split(sep=None, maxsplit=3)
    if len(listDevInfo) > 0: dictDevInfo["BYTESIZE"] = int(listDevInfo[0])
    if len(listDevInfo) > 1: dictDevInfo["VENDOR"] = listDevInfo[1]
    if len(listDevInfo) > 2: dictDevInfo["SERIAL"] = listDevInfo[2]
    if len(listDevInfo) > 3: dictDevInfo["MODEL"] = listDevInfo[3].strip()
    #
    try:
        # get detailed information for device
        # e.g. returns;
        #    Disk /dev/sdb: 931.5 GiB, 1000204886016 bytes, 1953525168 sectors
        #    Units: sectors of 1 * 512 = 512 bytes
        #    Sector size (logical/physical): 512 bytes / 4096 bytes
        #    I/O size (minimum/optimal): 4096 bytes / 4096 bytes
        #    Disklabel type: gpt
        #    Disk identifier: 12698CFE-ACB3-41FD-8FA2-89142B8C5CE4
        #    ...[final lines ignored]
        DevInfo = sh.sfdisk("-l", "--bytes", DevAddr)
    except ErrorReturnCode:
        ExitCode = str(DevInfo.exit_code)
        print("ERROR: sfdisk in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
        sys.exit(1)
    listDevInfo = DevInfo.splitlines()
    listLine = listDevInfo[0].split(sep=None)
    if len(listLine) > 2: dictDevInfo["GIBISIZE"] = float(listLine[2])
    if len(listLine) > 6: dictDevInfo["SECTORS"] = int(listLine[6])
    listLine = listDevInfo[1].split(sep=None)
    if len(listLine) > 7: dictDevInfo["SECTSIZE"] = int(listLine[7])
    listLine = listDevInfo[2].split(sep=None)
    if len(listLine) > 3: dictDevInfo["LOGSECT"] = int(listLine[3])
    if len(listLine) > 6: dictDevInfo["PHYSECT"] = int(listLine[6])
    listLine = listDevInfo[4].split(sep=None)
    if len(listLine) > 2: dictDevInfo["LABELTYPE"] = listLine[2]
    listLine = listDevInfo[5].split(sep=None)
    if len(listLine) > 2: dictDevInfo["UUID"] = listLine[2]
    return dictDevInfo
#
#
#
def fnGetPartitions(Device):
    dictPartitions = {}
    DevAddr = "/dev/"+Device
    #
    def fnStartsWithDevAddr(listItem):
        if len(listItem) > 0:
            return listItem.startswith(DevAddr)
        return False
    #
    dictFSInfo = fnGetFSInfo(DevAddr)
    #
    try:
        PartInfo = sh.sfdisk("-l", "--bytes", DevAddr)
        # get detailed information for device
        # e.g. returns;
        #    ...[initial lines filtered out]
        #    /dev/sdb1        2048  419432447 419430400 214748364800 Linux filesystem
        #    /dev/sdb2   419432448  524290047 104857600  53687091200 Linux filesystem
        #    /dev/sdb3   524290048  734005247 209715200 107374182400 Linux filesystem
        #    /dev/sdb4   734005248  838862847 104857600  53687091200 Linux filesystem
        #    /dev/sdb5  1951426560 1953523711   2097152   1073741824 EFI System
        #    /dev/sdb6  1638950912 1951426559 312475648 159987531776 Linux filesystem
        #    ...[final lines filtered out]
    except ErrorReturnCode:
        ExitCode = str(PartInfo.exit_code)
        print("ERROR: sfdisk in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
        sys.exit(1)
    listPartitions = filter(fnStartsWithDevAddr, PartInfo.splitlines())
    for PartitionLine in listPartitions:
        dictPartition = {}
        listLine = PartitionLine.split(sep=None)
        dictPartition["NAME"] = listLine[0].replace('/dev/', '')
        dictPartition["START"] = int(listLine[1])
        dictPartition["END"] = int(listLine[2])
        dictPartition["BYTES"] = int(listLine[4])
        dictPartition["TYPE"] = listLine[5]
        listFSInfo = dictFSInfo[dictPartition["NAME"]]
        dictPartition["FSTYPE"] = listFSInfo[0]
        dictPartition["FSLABEL"] = listFSInfo[1]
        dictPartition["MOUNT"] = listFSInfo[2]
        #
        listUsageInfo = fnGetUsageInfo(dictPartition)
        dictPartition["USED"] = listUsageInfo[0]
        dictPartition["PCENT"] = listUsageInfo[1]
        dictPartition["AVAIL"] = listUsageInfo[2]
        #
        dictPartitions[dictPartition["START"]] = dictPartition
    #
    listFreeSpace = fnGetFreeSpace(DevAddr)
    if len(listFreeSpace) > 0:
        for dictFreeSpace in listFreeSpace:
            dictPartitions[dictFreeSpace["START"]] = dictFreeSpace
    #
    return dictPartitions
#
#
#
def fnGetUsageInfo(dictPartition):
    listUsageInfo = []
    if dictPartition["FSTYPE"] == "swap":
        #
        #  swap partition
        #
        try:
            SwapInfo = sh.swapon(show="SIZE,USED", bytes=True)
            # get detailed information for device
            # e.g. returns;
            #        SIZE USED
            # 17179865088    0
        except ErrorReturnCode:
            ExitCode = str(SwapInfo.exit_code)
            print("ERROR: swapon in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
            sys.exit(1)
        listSwapInfo = SwapInfo.splitlines()
        listSwapLine = listSwapInfo[1].split(sep=None)
        Used = int(listSwapLine[1])
    #
    elif dictPartition["TYPE"] == "free":
        #
        #  unallocated space
        #
        Used = 0
    #
    else:
        #
        #  anything else
        #
        with tempfile.TemporaryDirectory() as MountPoint:
                Used = fnGetUsedSpace(MountPoint, dictPartition["NAME"])
    Avail = dictPartition["BYTES"] - Used
    PCent = Used/dictPartition["BYTES"]
    return [Used, PCent, Avail]
#
def fnGetUsedSpace(MountPoint,Device):
    DevAddr = "/dev/"+Device
    try:
        MountInfo = sh.mount("-v", DevAddr, MountPoint)
        #   mount: /dev/sdb? mounted on /???/????/????.
    except ErrorReturnCode:
        ExitCode = str(MountInfo.exit_code)
        print("ERROR: mount in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
        sys.exit(1)
    finally:
        try:
            DFInfo = sh.df("-B1", MountPoint)
            # e.g. returns
            #  Filesystem       1B-blocks       Used   Available Use% Mounted on
            #  /dev/sdb2      52710469632 3136086016 46873251840   7% /home/swillber/Media/Pictures
        except ErrorReturnCode:
            ExitCode = str(DFInfo.exit_code)
            print("ERROR: df in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
            sys.exit(1)
        finally:
            listDFInfo = DFInfo.splitlines()
            listPartInfo = listDFInfo[1].split(sep=None)
            Used = int(listPartInfo[2])
            while os.path.ismount(MountPoint):
                try:
                    UmountInfo = sh.umount("-v", MountPoint)
                    # umount: /tmp/xxxxx unmounted
                except ErrorReturnCode:
                    ExitCode = str(UmountInfo.exit_code)
                    print("ERROR: umount in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
                    sys.exit(1)
    return Used
#
def fnGetFSInfo(DevAddr):
    #
    def fnStartsWithPart(listItem):
        if len(listItem) > 0:
            return listItem.startswith("part")
        return False
    #
    dictFSInfo = {}
    #
    try:
        PartInfo = sh.lsblk("-lbno", "TYPE,NAME,FSTYPE,LABEL,MOUNTPOINT", DevAddr)
        # get additional partition data
        # e.g. returns;
        #   disk sda
        #   part sda1 vfat   EFISYS    /boot
        #   part sda2 ext4   ARCHLINUX /
        #   part sda3 swap   SWAP      [SWAP]
    except ErrorReturnCode:
        ExitCode = str(PartInfo.exit_code)
        print("ERROR: lsblk in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
        sys.exit(1)
    listPartitions = filter(fnStartsWithPart, PartInfo.splitlines())
    for PartitionLine in listPartitions:
        listLine = PartitionLine.split(sep=None)
        if len(listLine)<3: listLine.append("--")
        if len(listLine)<4: listLine.append("--")
        if len(listLine)<5: listLine.append("")
        dictFSInfo[listLine[1]] = [listLine[2],listLine[3],listLine[4]]
    return dictFSInfo
#
#
#
def fnGetFreeSpace(DevAddr):
    try:
        FreeSpace = sh.sfdisk("-lF", "--bytes", DevAddr)
        # list unallocated space on specified device
        # e.g. reurns;
        #   Unpartitioned space /dev/sdb: 381.5 GiB, 409645088768 bytes, 800088064 sectors
        #   Units: sectors of 1 * 512 = 512 bytes
        #   Sector size (logical/physical): 512 bytes / 4096 bytes
        #
        #       Start        End   Sectors         Size
        #   838862848 1638950911 800088064 409645088768
    except ErrorReturnCode as Err:
        ExitCode = str(FreeSpace.exit_code)
        print("ERROR: sfdisk in "+sys._getframe().f_code.co_name+" returned - "+ExitCode)
        sys.exit(1)
    listFreeSpace = []
    listFree = FreeSpace.splitlines()
    for Line in listFree:
        dictFreeSpace = {}
        Line = Line.lstrip()
        if len(Line) > 0:
            if Line[0].isdigit():
                listLine = Line.split(sep=None)
                dictFreeSpace["START"] = int(listLine[0])
                dictFreeSpace["END"] = int(listLine[1])
                dictFreeSpace["BYTES"] = int(listLine[3])
                dictFreeSpace["TYPE"] = "free"
                dictFreeSpace["NAME"] = "--"
                dictFreeSpace["FSTYPE"] = "--"
                dictFreeSpace["FSLABEL"] = "unallocated"
                dictFreeSpace["MOUNT"] = "--"
                dictFreeSpace["USED"] = 0
                dictFreeSpace["PCENT"] = 0
                dictFreeSpace["AVAIL"] = dictFreeSpace["BYTES"]
                listFreeSpace.append(dictFreeSpace)
    return listFreeSpace
#
#
#
def fnPrintDeviceInfo(Device, dictDevice):
#  ==>DEVICE: /dev/sdb          Vendor/Model: ATA/WDC WD10JPVX-60J
#     Serial: WD-WXL1E557LV7X        Disk Id: 12698CFE-ACB3-41FD-8FA2-89142B8C5CE4
#  Labeltype: GPT                       Size: 1000204886016 bytes (931.5 GiB)
#    Sectors: 1953525168         Sector Size: 512/4096 bytes (logical/physical)
    LINE1 = "\n ===>DEVICE: /dev/{:<12}Vendor/Model: {}/{}"
    LINE2 = "     Serial: {:<22}Disk Id: {}"
    LINE3 = " Label Type: {:<25}Size: {:,} bytes ({} GiB)"
    LINE4 = "    Sectors: {:<18,}Sector Size: {}/{} bytes (logical/physical)"
    #
    Vendor = dictDevice["VENDOR"]
    Model = dictDevice["MODEL"]
    Serial = dictDevice["SERIAL"]
    DiskId = dictDevice["UUID"]
    LabelType = dictDevice["LABELTYPE"].upper()
    Bytes = dictDevice["BYTESIZE"]
    GiBiSize = dictDevice["GIBISIZE"]
    Sectors = dictDevice["SECTORS"]
    Logical = dictDevice["LOGSECT"]
    Physical = dictDevice["PHYSECT"]
    #
    print(LINE1.format(Device, Vendor, Model))
    print(LINE2.format(Serial, DiskId))
    print(LINE3.format(LabelType, Bytes, GiBiSize))
    print(LINE4.format(Sectors, Logical, Physical))
#
#
#
def fnPrintPartitionInfo(dictPartitions):
    HDR1 = "\nPARTITION LABEL          TYPE          SIZE          USED              AVAILABLE"
    HDR2 =   "--------- -------------- ---- ------------- ---------------------- -------------"
    LINE = " {:^7}  {:<14} {:^4} {:>13} {:>13}({:>7.2%}) {:>13} "
    print("{}".format(HDR1))
    print("{}".format(HDR2))
    # dictionary keys are the partition start sector
    listStartSector = sorted(dictPartitions.keys())
    for StartSector in listStartSector:
        dictPartition = dictPartitions[StartSector]
        Name = dictPartition["NAME"]
        Label = dictPartition["FSLABEL"]
        FSType = dictPartition["FSTYPE"]
        Size = dictPartition["BYTES"]
        Used = dictPartition["USED"]
        PCent = dictPartition["PCENT"]
        Free = dictPartition["AVAIL"]
        print(LINE.format(Name, Label, FSType, Size, Used, PCent, Free))
#
#
#
def fnGetTargetDevice():
    Description = "Report on block storage device."
    ArgHelp = "block device name, e.g. sda or /dev/sda"
    #
    cmdline = argparse.ArgumentParser(description=Description)
    cmdline.add_argument('-d','--device', required=True, help=ArgHelp)
    #
    try:
        args = cmdline.parse_args()
    except SystemExit:
        sys.exit(99)
    #
    Device = args.device
    exit(2)
    return Device
#
#-------------------------------------------------------------------------------
#                   M A I N   L O G I C   F U N C T I O N
#-------------------------------------------------------------------------------
#
#
#-------------------------------------------------------------------------------
def fnMain():
    TgtDevice = fnGetTargetDevice()
    dictDevices = {}
    dictPartitions = {}
    listDevices = sorted(fnGetDevicesList())
    for Device in listDevices:
        dictDevices[Device] = fnGetDeviceData(Device)
        dictPartitions[Device] = fnGetPartitions(Device)
    for Device in sorted(dictDevices.keys()):
        fnPrintDeviceInfo(Device, dictDevices[Device])
        fnPrintPartitionInfo(dictPartitions[Device])
#
#===============================================================================
# if being run directly invoke main function
#===============================================================================
#
if __name__ == '__main__':
    try:
        fnMain()
    except SystemExit:
        # get the value of the exit argument
        Etype, Evalue, Etb = sys.exc_info()
        # get singleton list of form: ['SystemExit: 99\n']
        listExitNum = traceback.format_exception_only(Etype, Evalue)[0]
        # split list at ':' and get last item in list
        ExitNum = listExitNum.split(":")[1]
        # trim of any trailing newlines
        ExitNum = ExitNum.splitlines()[0]
        # convert to integer
        ExitNum = int(ExitNum)
        exit(ExitNum)
    except RuntimeError as runerror:
        print(runerror)
    except:
        Etype, Evalue, Etb = sys.exc_info()
        print('Unexpected Error - {}'.format(traceback.format_exception_only(Etype, Evalue)[0]), end='')
        Elist = traceback.extract_tb(Etb,-1)
        print('{}.{}() "{} {}"'.format(Elist[0].filename, Elist[0].name, Elist[0].lineno, Elist[0].line))
        exit(1)
